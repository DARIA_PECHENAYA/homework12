document.getElementById("burger").onclick = function() {myFunction()};

function myFunction() {
  document.getElementById("burger-window").classList.toggle("show");
};

document
    .querySelectorAll('.text-link, .text-link-cross')
    .forEach(link => link.onclick = toggleTile);

function toggleTile(event) {
    let dataParentClass = event.target.dataset["parentClass"];
    let parentSelector = `.${dataParentClass}.tile-green, .${dataParentClass}.tile-gray`;
    let tiles = document.querySelectorAll(parentSelector);
    tiles.forEach(tile => tile.classList.toggle("tile-show"));
};